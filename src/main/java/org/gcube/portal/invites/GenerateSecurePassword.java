package org.gcube.portal.invites;

import java.security.SecureRandom;
import java.util.Random;
 
public class GenerateSecurePassword {

    private static final Random RANDOM = new SecureRandom();
    private static final String ALPHABET = "123ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static String generatePassword(int length) {
        StringBuilder returnValue = new StringBuilder(length);

        for (int i = 0; i < length; i++) {
            returnValue.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
        }

        return new String(returnValue);
    }
}
