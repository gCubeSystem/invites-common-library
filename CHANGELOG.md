
# Changelog for invites-common-library

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.5.1] - 2020-07-21

Ported to git

Feature #19708 send a temporary password in the invite email if the account does not exists

## [v1.5.0] - 2017-11-29

Invite method revised to avoid double step in case of user registration

Invite emails are now sent via Email Templates Library

removed ASL Session, ported to gCube Client Context

## [v1.1.0] - 2016-07-30

Update for Liferay 6.2.5

## [v1.0.0] - 2015-06-30

First release 
